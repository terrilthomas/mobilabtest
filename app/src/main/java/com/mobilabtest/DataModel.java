package com.mobilabtest;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Terril-Den on 5/1/17.
 */

public class DataModel implements Parcelable {

    private String id;
    private String title;
    private int cover_width;
    private int cover_height;
    private String description;
    private int ups;
    private int downs;


    protected DataModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        cover_width = in.readInt();
        cover_height = in.readInt();
        ups = in.readInt();
        downs = in.readInt();
        description = in.readString();
    }

    public static final Creator<DataModel> CREATOR = new Creator<DataModel>() {
        @Override
        public DataModel createFromParcel(Parcel in) {
            return new DataModel(in);
        }

        @Override
        public DataModel[] newArray(int size) {
            return new DataModel[size];
        }
    };

    public int getCover_width() {
        return cover_width;
    }

    public int getCover_height() {
        return cover_height;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getDowns() {
        return downs;
    }

    public int getUps() {
        return ups;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeInt(cover_width);
        dest.writeInt(cover_height);
        dest.writeInt(ups);
        dest.writeInt(downs);
        dest.writeString(description);
    }
}
