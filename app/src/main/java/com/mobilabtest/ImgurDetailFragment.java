package com.mobilabtest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import static com.mobilabtest.MainActivity.IMAGEURL_POSTFIX;
import static com.mobilabtest.MainActivity.IMAGEURL_PREFIX;
import static com.mobilabtest.MainActivity.PARCEABLE_DATA;

/**
 * Created by Terril-Den on 5/3/17.
 */

public class ImgurDetailFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_load_data, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        DataModel model = bundle.getParcelable(PARCEABLE_DATA);

        ImageView dataImage = (ImageView) view.findViewById(R.id.imv_imgurDetail);
        TextView dataTitle = (TextView) view.findViewById(R.id.tv_imgurTitle);
        TextView dataDesc = (TextView) view.findViewById(R.id.tv_imgurDesc);
        TextView dataLike = (TextView) view.findViewById(R.id.tv_imgurLike);
        TextView dataDisLike = (TextView) view.findViewById(R.id.tv_imgurDislike);

        Glide.with(getContext())
                .load(IMAGEURL_PREFIX + model.getId() + IMAGEURL_POSTFIX)
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .crossFade()
                .into(dataImage);

        dataTitle.setText(model.getTitle());
        dataDesc.setText(model.getDescription());
        dataLike.setText("Likes (" + model.getUps() + ")");
        dataDisLike.setText("Dislike (" + model.getDowns() + ")");

    }
}
