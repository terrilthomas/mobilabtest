package com.mobilabtest;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MobiLabtest";

    private List<DataModel> mDataImgurList = Collections.emptyList();
    private int page = 0;
    private RecyclerView mRecylerView;
    private ImageAdapter mImageAdapter;
    private GridLayoutManager mLayoutManager = new GridLayoutManager(this, 2);
    private final String PARCED_DATA = "parcedData";
    public static final String PARCEABLE_DATA = "parceableData";
    public static final String IMAGEURL_PREFIX = "http://i.imgur.com/";
    public static final String IMAGEURL_POSTFIX = "m.jpg";
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(PARCED_DATA, (ArrayList) mDataImgurList);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onRestoreInstanceState(savedInstanceState);
        mDataImgurList = savedInstanceState.getParcelableArrayList(PARCED_DATA);
    }

    private void init(Bundle savedInstanceState) {
        mRecylerView = (RecyclerView) findViewById(R.id.recylerView);

        mRecylerView.setHasFixedSize(true);
        mRecylerView.setLayoutManager(mLayoutManager);

        mImageAdapter = new ImageAdapter();
        mRecylerView.setAdapter(mImageAdapter);

        mRecylerView.addOnScrollListener(mScrollListener);
        if (savedInstanceState == null) {

            mDialog = new ProgressDialog(MainActivity.this);
            mDialog.setTitle("Loading ...");
            mDialog.show();

            callImgurAPI();
        }
    }

    private void callImgurAPI() {
        ImgurAPIService service = ImgurRetrofitServiceFactory.createRetrofitService(ImgurAPIService.class, Commons.URL);
        service.getImgurApiData(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ImgurApiModel>() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete()");
                        if (mDialog != null)
                            mDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError()" + e.getMessage());

                    }

                    @Override
                    public void onNext(ImgurApiModel lotUtil) {
                        Log.d(TAG, lotUtil.getData().size() + "");
                        mDataImgurList = lotUtil.getData();
                        mImageAdapter.notifyDataSetChanged();
                    }

                });
    }

    private class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.view_adapter, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Glide.with(getBaseContext())
                    .load(IMAGEURL_PREFIX + mDataImgurList.get(position).getId() + IMAGEURL_POSTFIX)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .crossFade()
                    .into(holder.mGalleryImage);
            holder.mGalleryName.setText(mDataImgurList.get(position).getTitle());
        }

        @Override
        public int getItemCount() {
            return mDataImgurList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            ImageView mGalleryImage;
            TextView mGalleryName;

            public ViewHolder(View itemView) {
                super(itemView);
                mGalleryImage = (ImageView) itemView.findViewById(R.id.imv_Gallery);
                mGalleryName = (TextView) itemView.findViewById(R.id.tv_Gallery);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                itemClick(v);
            }
        }

        private void itemClick(View v) {
            int position = mRecylerView.getChildAdapterPosition(v);
            Bundle bundle = new Bundle();
            bundle.putParcelable(PARCEABLE_DATA, mDataImgurList.get(position));
            Fragment fragment = new ImgurDetailFragment();
            fragment.setArguments(bundle);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.frameContainer, fragment);
            transaction.addToBackStack(null);
            transaction.commit();

        }
    }

    private EndlessRecyclerOnScrollListener mScrollListener = new EndlessRecyclerOnScrollListener(MainActivity.this, mLayoutManager) {
        @Override
        public void onLoadMore(int currentPage) {
            Log.d(TAG, "Current Page :" + currentPage);
            page = currentPage;
            callImgurAPI();
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRecylerView.removeOnScrollListener(mScrollListener);
    }
}
