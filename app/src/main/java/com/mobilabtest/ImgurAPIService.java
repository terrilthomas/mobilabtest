package com.mobilabtest;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Terril-Den on 5/1/17.
 */

public interface ImgurAPIService {

        @GET("{page}.json")
        Observable<ImgurApiModel> getImgurApiData(@Path("page") int page);

}
